#ifdef FXUSB3
#include "cyu3system.h"
#include "cyc3.h"
#endif

#ifdef STM32F7
#include "stm32f7xx_hal.h"
#endif

#ifdef FTDI
	
#endif


#ifndef AD9826_H
#define AD9826_H

typedef struct
{
	void 			(*adsp_write)						(const uint8_t *buf_ptr);
	uint16_t 	(*adsp_read_reg)				(const uint8_t addr);
	void 			(*adsp_write_reg)				(const uint8_t addr, const uint16_t data);
	float 		(*adsp_calc_gain)				(const uint8_t gain);
	float 		(*adsp_calc_offset)			(const int16_t offset);
	int8_t 		(*adsp_compare_read)		(const uint8_t addr);	
  void 			(*adsp_init_reg_cache)	(void);
} const ad9826_TypeDef;

#ifdef FTDI
	#define AD9826_READ_PIN_PORT 		
	#define AD9826_CS_PORT 					
	#define AD9826_CLK_PORT 					
	#define AD9826_DATA_PORT 				

	#define AD9826_READ_PIN 			
	#define AD9826_CS_PIN						
	#define AD9826_CLK_PIN 					
	#define AD9826_DATA_PIN 			

	#define AD9826_CS_1 					
	#define AD9826_CS_0 					
	#define AD9826_CLK_1 				
	#define AD9826_CLK_0 				
	#define AD9826_DATA_1 				
	#define AD9826_DATA_0 			
#endif

#ifdef FXUSB3
	#define AD9826_READ_PIN 			CYC3_OUT_DSP_PIN

	#define AD9826_CS_1 					CYC3_SLOAD_1
	#define AD9826_CS_0 					CYC3_SLOAD_0
	#define AD9826_CLK_1 					CYC3_SCLK_1
	#define AD9826_CLK_0 					CYC3_SCLK_0
	#define AD9826_DATA_1 				CYC3_SDATA_1
	#define AD9826_DATA_0 				CYC3_SDATA_0
#endif

#ifdef STM32F7
	#define AD9826_READ_PIN_PORT 		GPIOD
	#define AD9826_CS_PORT 					GPIOD
	#define AD9826_CLK_PORT 				GPIOD	
	#define AD9826_DATA_PORT 				GPIOD

	#define AD9826_READ_PIN 			GPIO_PIN_0
	#define AD9826_CS_PIN					GPIO_PIN_0	
	#define AD9826_CLK_PIN 				GPIO_PIN_0	
	#define AD9826_DATA_PIN 			GPIO_PIN_0

	#define AD9826_CS_1 					HAL_GPIO_WritePin(AD9826_CS_PORT, AD9826_CS_PIN, GPIO_PIN_SET);
	#define AD9826_CS_0 					HAL_GPIO_WritePin(AD9826_CS_PORT, AD9826_CS_PIN, GPIO_PIN_RESET);
	#define AD9826_CLK_1 					HAL_GPIO_WritePin(AD9826_CLK_PORT, AD9826_CLK_PIN, GPIO_PIN_SET);
	#define AD9826_CLK_0 					HAL_GPIO_WritePin(AD9826_CLK_PORT, AD9826_CLK_PIN, GPIO_PIN_RESET);
	#define AD9826_DATA_1 				HAL_GPIO_WritePin(AD9826_DATA_PORT, AD9826_DATA_PIN, GPIO_PIN_SET);
	#define AD9826_DATA_0 				HAL_GPIO_WritePin(AD9826_DATA_PORT, AD9826_DATA_PIN, GPIO_PIN_RESET);
#endif

/**
  * ----- Internal Register Map
  **/
#define AD9826_CONFIG 			(uint8_t)(0)
#define AD9826_MUXCFG 			(uint8_t)(1)
#define AD9826_REDPGA 			(uint8_t)(2)
#define AD9826_GREENPGA 		(uint8_t)(3)
#define AD9826_BLUEPGA 			(uint8_t)(4)
#define AD9826_REDOFFSET 		(uint8_t)(5)
#define AD9826_GREENOFFSET 		(uint8_t)(6)
#define AD9826_BLUEOFFSET 		(uint8_t)(7)

/**
  * ----- Configuration Register Settings
  **/
#define AD9826_CONFIG_4V 									(uint8_t)(1 << 7)
#define AD9826_CONFIG_2V 									(uint8_t)(~(1 << 7))
#define AD9826_CONFIG_VREF_ON 						(uint8_t)(1 << 6)
#define AD9826_CONFIG_VREF_OFF 						(uint8_t)(~(1 << 6))
#define AD9826_CONFIG_VREF_3CH_ON 				(uint8_t)(1 << 5)
#define AD9826_CONFIG_VREF_3CH_OFF				(uint8_t)(~(1 << 5))
#define AD9826_CONFIG_VREF_CDS_ON 				(uint8_t)(1 << 4)
#define AD9826_CONFIG_VREF_CDS_OFF				(uint8_t)(~(1 << 4))
#define AD9826_CONFIG_VREF_CLAMP_4V 			(uint8_t)(1 << 3)
#define AD9826_CONFIG_VREF_CLAMP_3V				(uint8_t)(~(1 << 3))
#define AD9826_CONFIG_VREF_PD_ON 					(uint8_t)(1 << 2)
#define AD9826_CONFIG_VREF_PD_OFF					(uint8_t)(~(1 << 2))
#define AD9826_CONFIG_VREF_OUTMODE_1BYTE 	(uint8_t)(1)
#define AD9826_CONFIG_VREF_OUTMODE_2BYTE	(uint8_t)(~(1 << 0))

/**
  * ----- MUX Configuration Register Settings
  **/
#define AD9826_MUX_ORDER_RGB 						(uint8_t)(1 << 7)
#define AD9826_MUX_ORDER_BGR 						(uint8_t)(~(1 << 7))
#define AD9826_MUX_VREF_CH_RED_ON 			(uint8_t)(1 << 6)
#define AD9826_MUX_VREF_CH_RED_OFF 			(uint8_t)(~(1 << 6))
#define AD9826_MUX_VREF_CH_GREEN_ON 		(uint8_t)(1 << 5)
#define AD9826_MUX_VREF_CH_GREEN_OFF 		(uint8_t)(~(1 << 5))
#define AD9826_MUX_VREF_CH_BLUE_ON 			(uint8_t)(1 << 4)
#define AD9826_MUX_VREF_CH_BLUE_OFF 		(uint8_t)(~(1 << 4))

/**
  * ----- PGA Gain Register Settings
  **/
#define AD9826_MUX_ORDER_GAIN_FULL 			(uint16_t)(0x3F)

/**
  * ----- Offset Register Settings
  **/
#define AD9826_MUX_ORDER_OFFSET_FULL 		(uint16_t)(0x1FF)
#endif
