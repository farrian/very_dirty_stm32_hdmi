#include "ad9826.h"

#ifdef FXUSB3
#include "cyu3utils.h"
#include "cyu3gpio.h"
#endif

#ifdef STM32F7
#include "stm32f7xx_hal.h"
	#ifdef RTE_CMSIS_RTOS                   // when RTE component CMSIS RTOS is used
	#include "cmsis_os.h"                   // CMSIS RTOS header file
	#endif
#endif

#ifdef FTDI
#endif

uint16_t ad9826RegArray[8] = {0, 1, 2, 3, 4, 5, 6, 7}; //cache for AD9826 registers, useful in read/write operations

/**
	*	-----	generate clk signal to ADSP
	**/
static void adsp_clk(void);

/**
	*	-----	write ADSP register with pointer to data
	*	buf_ptr - pointer to array with data to write
	**/
static void adsp_write(const uint8_t *buf_ptr);

/**
	*	-----	read ADSP register
	*	addr - register adress
	*	return read data
	**/
static uint16_t adsp_read_reg(const uint8_t addr);

/**
	*	-----	write ADSP register with addr and data
	*	addr - register adress
	*	data - data to write
	**/
static void adsp_write_reg(const uint8_t addr, const uint16_t data);

/**
	*	-----	calculate the input signal gain
	*	gain - gain data with range from 0 to 63
	*	return gain coefficient (V/V)
	**/
static float adsp_calc_gain(const uint8_t gain);

/**
	*	-----	calculate the input signal offset
	*	offset - offset data with range from -255 to +255
	*	return offset
	**/
static float adsp_calc_offset(const int16_t offset);

/**
	*	-----	compare the writed and readed data
	*	addr - register adress
	*	return 0 - if all is OK
	*	return  - if error detected
	**/
static int8_t adsp_compare_read(const uint8_t addr);

/**
	*	-----	read all adsp registers and save them into cache(ad9826RegArray)
	**/
static void adsp_init_reg_cache(void);

ad9826_TypeDef ad9826 =
{
	adsp_write,
	adsp_read_reg,
	adsp_write_reg,
	adsp_calc_gain,
	adsp_calc_offset,
	adsp_compare_read,
	adsp_init_reg_cache
};

/**
	***********
	**/
static void adsp_init_reg_cache(void)
{
	uint8_t i = 0;
	
	for(i = 0; i < 8; i++)
	{
			ad9826RegArray[i] = adsp_read_reg(i);
	}
}

/**
	***********
	**/
static int8_t adsp_compare_read(const uint8_t addr)
{
	uint16_t dataR = 0;
	
	dataR = adsp_read_reg(addr);
	
	if(ad9826RegArray[addr] != dataR)
	{
			//Send error func to GUI
		return 1;
	}		
	
	return 0;
}

/**
	***********
	**/
static float adsp_calc_offset(const int16_t offset)
{
	float mV = 0.0;	
	
	mV = offset * 1.171875;
	
	return mV;
}


/**
	***********
	**/
static float adsp_calc_gain(const uint8_t gain)
{
	float c = 0.0;
	
	c = 6.0 / (1.0 + 5.0 * ((63.0 - gain) / 63.0));
	
	return c;
}

/**
	***********
	**/
static void adsp_write_reg(const uint8_t addr, const uint16_t data)
{
	uint16_t dataW = (addr << 11) | (0x1FF & data);
	
	ad9826RegArray[addr] = 0x1FF & data;	
	
	adsp_write((uint8_t*)(&dataW));	
}

/**
	***********
	**/
static uint16_t adsp_read_reg(const uint8_t addr)
{
	uint8_t i = 0;
	uint16_t shift = 1 << 2;
	uint8_t dataWrite = 0;
	uint16_t dataRead = 0;
	
#ifdef FXUSB3
	CyBool_t bitRead = CyFalse;
#endif
#ifdef STM32F7
	uint8_t bitRead = 0;
#endif
#ifdef FTDI
	uint8_t bitRead = 0;
#endif

	dataWrite = addr;

	AD9826_CS_1;
	
#ifdef FXUSB3
	CyU3PBusyWait(1);
#endif
#ifdef STM32F7
	#ifdef RTE_CMSIS_RTOS                
		osDelay(1);
	#endif
#endif
#ifdef FTDI
	//DELAY_1ms
#endif

	AD9826_DATA_1;	//обозначение чтения
	adsp_clk();

	for(i = 0; i < 15; i++)
	{
		if(i < 3)
		{
			if((dataWrite & shift) != 0)
			{
				AD9826_DATA_1;
			}
			else
			{
				AD9826_DATA_0;
			}

			if(shift == 1)
			{
				shift = 1 << 8;
			}
			else
			{
				shift >>= 1;
			}
		}
		else
		{
			if((i >= 3) && (i <= 5))
			{
				AD9826_DATA_0;
			}
			else
			{
				#ifdef FXUSB3
				CyU3PGpioSimpleGetValue(AD9826_READ_PIN, &bitRead);
				#endif
				#ifdef STM32F7
				bitRead = HAL_GPIO_ReadPin(AD9826_READ_PIN_PORT, AD9826_READ_PIN);
				#endif
				#ifdef FTDI
				//bitRead = READ_BIT
				#endif
				dataRead |= bitRead << (15 - i - 1);
			}
		}

		adsp_clk();
	}

	AD9826_CS_0;
	
#ifdef FXUSB3
	CyU3PBusyWait(1);
#endif
#ifdef STM32F7
	#ifdef RTE_CMSIS_RTOS                
		osDelay(1);
	#endif
#endif
#ifdef FTDI
	//DELAY_1ms
#endif

	return dataRead;
}

/**
	***********
	**/
static void adsp_write(const uint8_t *buf_ptr)
{
	uint8_t i = 0;
	uint16_t shift = 1 << 11;
	uint16_t dataWrite = 0;

	dataWrite = buf_ptr[0] | (buf_ptr[1] << 9);

	AD9826_CS_1;
	
#ifdef FXUSB3
	CyU3PBusyWait(1);
#endif
#ifdef STM32F7
	#ifdef RTE_CMSIS_RTOS                
		osDelay(1);
	#endif
#endif
#ifdef FTDI
	//DELAY_1ms
#endif
	
	AD9826_DATA_0;	//обозначение записи
	adsp_clk();

	for(i = 0; i < 15; i++)
	{
		if((i >= 3) && (i <= 5))
		{
			AD9826_DATA_0;
		}
		else
		{
			if((dataWrite & shift) != 0)
			{
				AD9826_DATA_1;
			}
			else
			{
				AD9826_DATA_0;
			}

			if(shift == 1)
			{
				shift = 1 << 11;
			}
			else
			{
				shift >>= 1;
			}
		}

		adsp_clk();
	}

	AD9826_CS_0;
	
#ifdef FXUSB3
	CyU3PBusyWait(1);
#endif
#ifdef STM32F7
	#ifdef RTE_CMSIS_RTOS                
		osDelay(1);
	#endif
#endif
#ifdef FTDI
	//DELAY_1ms
#endif
}

/**
	***********
	**/
static void adsp_clk(void)
{
#ifdef FXUSB3
	CyU3PBusyWait(1);
#endif
#ifdef STM32F7
	#ifdef RTE_CMSIS_RTOS                
		osDelay(1);
	#endif
#endif
#ifdef FTDI
	//DELAY_1ms
#endif
	
	AD9826_CLK_1;
	
#ifdef FXUSB3
	CyU3PBusyWait(1);
#endif
#ifdef STM32F7
	#ifdef RTE_CMSIS_RTOS                
		osDelay(1);
	#endif
#endif	
#ifdef FTDI
	//DELAY_1ms
#endif
	AD9826_CLK_0;
}
