
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "stdio.h"                                           // CMSIS RTOS header file
#include "Board_Touch.h"                                           // CMSIS RTOS header file
#include "GUI.h"                                           // CMSIS RTOS header file
#include "rl_usb.h"
#include "HDMI.h"

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void Touch_Thread (void const *argument);                             // thread function
osThreadId tid_Touch_Thread;                                          // thread id
osThreadDef (Touch_Thread, osPriorityIdle, 1, 0);                   // thread object

extern void Touch_Callibrate(void);
extern void Touch_clear(void);

int Init_Touch_Thread (void) {

  tid_Touch_Thread = osThreadCreate (osThread(Touch_Thread), NULL);
  if (!tid_Touch_Thread) return(-1);
  
  return(0);
}

void Touch_Thread (void const *argument) 
{	
  usbStatus usb_status;                 // USB status
	usbHID_MouseState MouseState;
	GUI_PID_STATE State;
  uint8_t   con = 0;                   // Connection status of keyboard
	int16_t dx = 0, dy = 0;
	int16_t x_log = 0, y_log = 0;	
	
	
	usb_status = USBH_Initialize (0);    // Initialize USB Host 00
  if (usb_status != usbOK)
	{
    while(1){}
  }
	
	//osSignalWait(1, osWaitForever); 
	
  while (1)
	{			
    usb_status = USBH_HID_GetDeviceStatus (0); // Get HID device status
    if (usb_status == usbOK)
		{
      if (con == 0U)
			{                 
        con = 1U; 
//				status_send("Mouse was found.");				
      }
    } 
		else
		{
      if (con == 1U)
			{                 
        con = 0U;                     
      }
			else
			{
//				status_send("Mouse was not found.");
			}
    }
    if (con != 0U)
		{      
			GUI_MOUSE_GetState(&State);	
      USBH_HID_GetMouseState(0, &MouseState);
			
			dx = MouseState.x - x_log;
			dy = MouseState.y - y_log;
			
			if(dx > 0) //движение вправо
			{
//				right = 1;
//				left = 0;	
				
				if((State.x + dx) > (HACT - 1))
				{
					State.x = HACT - 1;
				}
				else
				{
					State.x += dx;
				}					
			}
			else //движение влево
			{
//				right = 0;
//				left = 1;	

				if((State.x + dx) < 0)
				{
					State.x = 0;
				}
				else
				{
					State.x += dx;
				}				
			}	

			if(dy > 0) //движение вниз
			{
//				down = 1;
//				up = 0;	
				
				if((State.y + dy) > (VACT - 1))
				{
					State.y = VACT - 1;
				}
				else
				{
					State.y += dy;
				}					
			}
			else //движение вверх
			{
//				up = 1;
//				down = 0;	

				if((State.y + dy) < 0)
				{
					State.y = 0;
				}
				else
				{
					State.y += dy;
				}				
			}			
			
			x_log = MouseState.x;
			y_log = MouseState.y;
			
			State.Pressed = 0;
			
			if(MouseState.button == 1)
			{
				State.Pressed = 1;
			}
			
			GUI_MOUSE_StoreState(&State);
    }		
		
    osDelay(10);
  }
} 
