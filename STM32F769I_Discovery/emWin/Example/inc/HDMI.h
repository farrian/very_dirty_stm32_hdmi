
#ifndef __HDMI_H
#define __HDMI_H

//#include "stm32f7xx_hal.h"
#include "stdint.h"

#define HDMI_1024X768 0
#define HDMI_1280X720 1

#define HDMI_FORMAT HDMI_1280X720

#if (HDMI_FORMAT == HDMI_1024X768)
/*********************************************************************
*
*       HDMI Format 1024*768 60Hz RGB565
*/

#define HACT   						1024
#define HSW   						13
#define HBP   						30
#define HFP   						20
#define VACT   						768
#define VSW   						4
#define VBP   						14
#define VFP   						1
#define FORMAT_SCREEN   	ADV7533_ASPECT_RATIO_4_3
#define FORMAT_RGB   			DSI_RGB565

/*********************************************************************
*
*       HDMI PLL configaration
*/
#define LCD_PLL_N   			410
#define LCD_PLL_DIV   		4
#define LCD_PLL_PCLK   		51250
#define DSI_PLL_IDF   		DSI_PLL_IN_DIV5
#define DSI_PLL_NDIV   		82
#define DSI_PLL_ODF   		DSI_PLL_OUT_DIV1
#define DSI_PLL_LBCLK   	51250
#define DSI_PLL_TXE   		3

/*********************************************************************
*
*       HDMI PLL configaration
*/

#define DSI_PACKET_NP   	0
#define DSI_PACKET_NC   	1
#define DSI_PACKET_VP   	HACT

#endif

#if (HDMI_FORMAT == HDMI_1280X720)
/*********************************************************************
*
*       HDMI Format 1280*720 60Hz RGB565
*/

#define HACT   						1280
#define HSW   					15
#define HBP   					30
#define HFP   					20
#define VACT   						720
#define VSW   						4
#define VBP   						14
#define VFP   						1
#define FORMAT_SCREEN   	ADV7533_ASPECT_RATIO_16_9
#define FORMAT_RGB   			DSI_RGB565

/*********************************************************************
*
*       HDMI PLL configaration
*/
#define LCD_PLL_N   			360
#define LCD_PLL_DIV   		3
#define LCD_PLL_PCLK   		60000
#define DSI_PLL_IDF   		DSI_PLL_IN_DIV5
#define DSI_PLL_NDIV   		96
#define DSI_PLL_ODF   		DSI_PLL_OUT_DIV1
#define DSI_PLL_LBCLK   	60000
#define DSI_PLL_TXE   		3


/*********************************************************************
*
*       HDMI PLL configaration
*/

#define DSI_PACKET_NP   	0
#define DSI_PACKET_NC   	1
#define DSI_PACKET_VP   	HACT

#endif

#endif
